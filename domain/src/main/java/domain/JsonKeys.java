package domain;

public class JsonKeys {

    public static final String APP_IP_ADDRESS = "app_ip_address";
    public static final String APP_PACKAGE = "app_package";
    public static final String APP_VERSION = "app_version";
    public static final String DEVICE_ID = "device_id";
    public static final String USER_AGENT = "user_agent";
    public static final String USER_AGENT_VERSION = "user_agent_version";
}
