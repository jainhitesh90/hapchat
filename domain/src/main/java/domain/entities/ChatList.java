package domain.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatList {

    @SerializedName("count")
    private int count;

    @SerializedName("messages")
    private List<ChatModel> chatModelList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ChatModel> getChatModelList() {
        return chatModelList;
    }

    public void setChatModelList(List<ChatModel> chatModelList) {
        this.chatModelList = chatModelList;
    }
}
