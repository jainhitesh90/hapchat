package domain.entities;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

public class ErrorModel {

    private boolean status = false;
    private String tag;
    private int statusCode = -1;
    private String errorMessage;
    private String errorType;

    public ErrorModel() {
    }


    private ErrorModel(int statusCode, String errorMsg) {
        this.statusCode = statusCode;
        this.errorMessage = errorMsg;
    }

    public ErrorModel(int statusCode, String errorMessage, String tag) {
        this(statusCode, errorMessage);
        this.tag = tag;
    }

    private ErrorModel(retrofit.Response response) {
        this.statusCode = response.code();
        this.errorMessage = response.message();

        try {
            String errorBodyString = response.errorBody().string();
            ResponseModel responseModel = new Gson().fromJson(new JsonParser().parse(errorBodyString), ResponseModel.class);
            if (responseModel.getError() != null) {
                this.errorMessage = responseModel.getError();
            } else {
                this.errorMessage = responseModel.getMessage();
            }
            this.errorType = responseModel.getErrorType();
        } catch (IOException | JsonSyntaxException | NullPointerException ignored) {
        }
    }

    public ErrorModel(retrofit.Response response, String tag) {
        this(response);
        this.tag = tag;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
}