package domain.entities;

import com.google.gson.annotations.SerializedName;

public class ResponseModel<S> {

    @SerializedName("data")
    private S data;
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("error_type")
    private String errorType;
    private int code;
    private String error;

    public String getStatus() {
        return status;
    }

    String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isStatus() {
        if (status == null) return false;
        try {
            return Boolean.parseBoolean(status);
        } catch (Exception e) {
            return false;
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public S getData() {
        return data;
    }

    String getErrorType() {
        return errorType;
    }

}
