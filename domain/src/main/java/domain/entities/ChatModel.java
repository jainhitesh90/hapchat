package domain.entities;

import com.google.gson.annotations.SerializedName;

public class ChatModel {

    @SerializedName("body")
    private String chatMessage;

    @SerializedName("username")
    private String userId;

    @SerializedName("Name")
    private String userName;

    @SerializedName("image-url")
    private String userImage;

    @SerializedName("message-time")
    private String chatMessageTime;

    private boolean markedFavorite = false;

    public String getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(String chatMessage) {
        this.chatMessage = chatMessage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getChatMessageTime() {
        return chatMessageTime;
    }

    public void setChatMessageTime(String chatMessageTime) {
        this.chatMessageTime = chatMessageTime;
    }

    public boolean isMarkedFavorite() {
        return markedFavorite;
    }

    public void setMarkedFavorite(boolean markedFavorite) {
        this.markedFavorite = markedFavorite;
    }
}
