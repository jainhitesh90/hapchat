package domain.rest;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import domain.Constants;
import domain.entities.ErrorModel;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public abstract class RestCall {

    <S> S createService(String baseUrl, Class<S> serviceClass, boolean isDebuggable) {
        OkHttpClient client = new OkHttpClient();
        if (isDebuggable) {
            client.interceptors().add(new Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    return chain.proceed(chain.request());
                }
            });
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(serviceClass);
    }

    void onError(Throwable t, String tag, Bus bus) {
        if (t instanceof UnknownHostException) {
            bus.post(new ErrorModel(0, Constants.NO_INTERNET_CONNECTION_MSG, tag));
        } else if (t instanceof ConnectException) {
            bus.post(new ErrorModel(999, Constants.PROBLEM_CONNECTING_MSG, tag));
        } else if (t instanceof TimeoutException) {
            bus.post(new ErrorModel(999, Constants.TIMEOUT_CONNECTION_MSG, tag));
        } else {
            bus.post(new ErrorModel(999, Constants.COMMON_ERROR_MSG, tag));
        }
    }
}
