package domain.rest;

import com.squareup.otto.Bus;
import domain.APICalls;
import domain.Constants;
import domain.entities.ErrorModel;
import domain.entities.ChatList;
import domain.entities.RequestUrl;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.GET;

public class ChatRestCall extends RestCall {

    private static final String GET_CHATS = "get_chats";
    private ApiService apiService;
    private Bus mBus;

    public ChatRestCall(RequestUrl requestUrl, Bus bus) {
        this.mBus = bus;
        String baseUrl = requestUrl.isDebug() ? Constants.TEST_BASE_URL : Constants.PROD_BASE_URL;
        apiService = createService(baseUrl, ApiService.class, requestUrl.isDebug());
    }

    public void fetchChatList() {
        Call<ChatList> call = apiService.fetchChatList();
        call.enqueue(new Callback<ChatList>() {
            @Override
            public void onResponse(Response<ChatList> response, Retrofit retrofit) {
                ChatList chatList = response.body();
                if (chatList != null) {
                    mBus.post(chatList);
                } else {
                    mBus.post(new ErrorModel(response, GET_CHATS));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                onError(t, GET_CHATS, mBus);
            }
        });
    }

    interface ApiService {

        @GET(APICalls.FETCH_CHAT_LIST)
        Call<ChatList> fetchChatList();

    }
}
