package example.com.hapchat.provider;

import android.content.ContentProvider;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import domain.Constants;

public abstract class BaseProvider extends ContentProvider {
    SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        DatabaseHelper dbHelper = new DatabaseHelper(getContext());
        db = dbHelper.getWritableDatabase();
        return (db != null);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        private static final String SQL_CREATE_CHATS =
                "CREATE TABLE " + ChatProvider.ChatColumns.TABLE_NAME + " (" +
                        ChatProvider.ChatColumns._ID + " INTEGER PRIMARY KEY, " +
                        ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE + " TEXT, " +
                        ChatProvider.ChatColumns.COLUMN_CHAT_FAVORITE + " INTEGER, " +
                        ChatProvider.ChatColumns.COLUMN_USER_ID + " TEXT, " +
                        ChatProvider.ChatColumns.COLUMN_USER_NAME + " TEXT, " +
                        ChatProvider.ChatColumns.COLUMN_USER_IMAGE + " TEXT, " +
                        ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE_TIME + " TEXT" + ");";

        private static final String SQL_DELETE_CHATS =
                "DROP TABLE IF EXISTS " + ChatProvider.ChatColumns.TABLE_NAME;

        private DatabaseHelper(Context context) {
            super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_CHATS);
        }


        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (oldVersion > 0) {
                db.execSQL(SQL_DELETE_CHATS);
            }
            onCreate(db);
        }
    }
}