package example.com.hapchat.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import com.example.hapchat.BuildConfig;

import example.com.hapchat.tool.SelectionBuilder;

public class ChatProvider extends BaseProvider {

    public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".provider.chats";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final int ROUTE_CHATS = 1;
    public static final int ROUTE_CHATS_ID = 2;
    private static final String PATH_CHATS = "chats";
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final String TAG = "ChatProvider";

    static {
        sUriMatcher.addURI(AUTHORITY, "chats", ROUTE_CHATS);
        sUriMatcher.addURI(AUTHORITY, "chats/*", ROUTE_CHATS_ID);
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_CHATS:
                return ChatColumns.CONTENT_TYPE;
            case ROUTE_CHATS_ID:
                return ChatColumns.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        SelectionBuilder builder = new SelectionBuilder(TAG);
        int uriMatch = sUriMatcher.match(uri);
        switch (uriMatch) {
            case ROUTE_CHATS_ID:
                // Return a single entry, by ID.
                String id = uri.getLastPathSegment();
                builder.where(ChatColumns._ID + "=?", id);
            case ROUTE_CHATS:
                // Return all known entries.
                builder.table(ChatColumns.TABLE_NAME).where(selection, selectionArgs);
                Cursor cursor = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                Context context = getContext();
                assert context != null;
                cursor.setNotificationUri(context.getContentResolver(), uri);
                return cursor;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * Insert a new entry into the database.
     */
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        assert db != null;
        final int match = sUriMatcher.match(uri);
        Uri result;
        switch (match) {
            case ROUTE_CHATS:
                long id = db.insertOrThrow(ChatColumns.TABLE_NAME, null, values);
                result = Uri.parse(ChatColumns.CONTENT_URI + "/" + id);
                break;
            case ROUTE_CHATS_ID:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context context = getContext();
        assert context != null;
        context.getContentResolver().notifyChange(uri, null, false);
        return result;
    }

    /**
     * Delete an entry by database by URI.
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder(TAG);
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_CHATS:
                count = builder.table(ChatColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            case ROUTE_CHATS_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(ChatColumns.TABLE_NAME)
                        .where(ChatColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context context = getContext();
        assert context != null;
        context.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * Update an entry in the database by URI.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder(TAG);
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_CHATS:
                count = builder.table(ChatColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_CHATS_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(ChatColumns.TABLE_NAME)
                        .where(ChatColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        Context context = getContext();
        assert context != null;
        context.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    public static class ChatColumns implements BaseColumns {

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.chatDB.chats";

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.chatDB.chats";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHATS).build();

        public static final String TABLE_NAME = "chats";

        public static final String _ID = "_id";

        public static final String COLUMN_CHAT_MESSAGE = "chat_message";

        public static final String COLUMN_CHAT_FAVORITE = "chat_favorite";

        public static final String COLUMN_USER_NAME = "user_name";

        public static final String COLUMN_USER_ID = "user_id";

        public static final String COLUMN_USER_IMAGE = "user_image";

        public static final String COLUMN_CHAT_MESSAGE_TIME = "chat_message_time";

    }
}