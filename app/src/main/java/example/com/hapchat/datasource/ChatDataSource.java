package example.com.hapchat.datasource;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import domain.entities.ChatModel;
import example.com.hapchat.provider.ChatProvider;
import example.com.hapchat.tool.HelperClass;
import example.com.hapchat.wrapper.ChatWrapper;

public class ChatDataSource {

    private Context mContext;

    public ChatDataSource(Context context) {
        this.mContext = context;
    }

    public void insert(ChatModel chatModel) {
        ChatModel prevChatModel = getChatModel(chatModel.getChatMessageTime());
        if (prevChatModel == null) {
            mContext.getContentResolver().insert(ChatProvider.ChatColumns.CONTENT_URI, ChatWrapper.modelToContent(chatModel));
        } else {
            chatModel.setMarkedFavorite(prevChatModel.isMarkedFavorite()); //restoring favorite marked items
            update(chatModel);
        }
    }

    public void update(ChatModel chatModel) {
        try {
            mContext.getContentResolver().update(ChatProvider.ChatColumns.CONTENT_URI, ChatWrapper.modelToContent(chatModel), ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE + "='" + chatModel.getChatMessage() + "' ", null);
        } catch (Exception e) {
            HelperClass.showMessage(mContext, "Error while updating");
        }
    }

    private ChatModel getChatModel(String chatMessageTime) {
        ChatModel chatModel = null;
        Cursor cursor = mContext.getContentResolver().query(ChatProvider.ChatColumns.CONTENT_URI, null, ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE_TIME + "='" + chatMessageTime + "'", null, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                chatModel = ChatWrapper.cursorToModel(cursor);
            }
            cursor.close();
        }
        return chatModel;
    }

    public String getUserImage(String userName) {
        String image = null;
        Cursor cursor = mContext.getContentResolver().query(ChatProvider.ChatColumns.CONTENT_URI, null, ChatProvider.ChatColumns.COLUMN_USER_NAME + "='" + userName + "'", null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            image = ChatWrapper.cursorToModel(cursor).getUserImage();
            cursor.close();
        }
        return image;
    }

    public List<ChatModel> getAllChats() {
        List<ChatModel> chatModelList = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(ChatProvider.ChatColumns.CONTENT_URI, null, null, null, ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE_TIME + " ASC");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                chatModelList.add(ChatWrapper.cursorToModel(cursor));
            }
            cursor.close();
        }
        return chatModelList;
    }

    public List<String> getAllUsers() {
        List<String> stringList = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(ChatProvider.ChatColumns.CONTENT_URI, new String[]{"DISTINCT " + ChatProvider.ChatColumns.COLUMN_USER_NAME}, null, null, ChatProvider.ChatColumns.COLUMN_USER_NAME);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String userName = cursor.getString(cursor.getColumnIndex(ChatProvider.ChatColumns.COLUMN_USER_NAME));
                stringList.add(userName);
            }
            cursor.close();
        }
        return stringList;
    }

    public int getAllChatFromUser(String userName) {
        int count = 0;
        Cursor cursor = mContext.getContentResolver().query(ChatProvider.ChatColumns.CONTENT_URI, null, ChatProvider.ChatColumns.COLUMN_USER_NAME + "= '" + userName + "' ", null, null);
        if (cursor != null && cursor.getCount() > 0) {
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }

    public int getAllFavoriteChatFromUser(String userName) {
        int count = 0;
        String selection1 = ChatProvider.ChatColumns.COLUMN_USER_NAME + "= '" + userName + "' ";
        String selection2 = ChatProvider.ChatColumns.COLUMN_CHAT_FAVORITE + "= " + 1 + " ";
        String selection = selection1 + " AND " + selection2;
        Cursor cursor = mContext.getContentResolver().query(ChatProvider.ChatColumns.CONTENT_URI, null, selection, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }

    public Cursor getSearchCursor(String s) {
        String column1 = ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE;
        Cursor c = mContext.getContentResolver().query(ChatProvider.ChatColumns.CONTENT_URI, null, column1 + " LIKE" + "'%" + s + "%'", null, ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE + " ASC ");
        if (c != null)
            c.moveToFirst();
        return c;
    }
}