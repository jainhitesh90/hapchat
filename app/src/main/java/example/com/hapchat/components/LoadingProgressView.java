package example.com.hapchat.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hapchat.R;
import com.pnikosis.materialishprogress.ProgressWheel;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoadingProgressView extends LinearLayout {

    @Bind(R.id.progress_wheel)
    protected ProgressWheel progressWheel;

    @Bind(R.id.progress_text)
    protected TextView mProgressText;

    public LoadingProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_load_progress, null);
        this.addView(view);
        ButterKnife.bind(this, view);
        setAttributes(attrs);
    }

    private void setAttributes(AttributeSet attributes) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attributes, R.styleable.LoadingProgressView, 0, 0);
        int color = typedArray.getInt(R.styleable.LoadingProgressView_loadingBarColor, ContextCompat.getColor(getContext(), R.color.theme_primary));
        String progressText = typedArray.getString(R.styleable.LoadingProgressView_progressText);
        typedArray.recycle();
        if (progressWheel != null) progressWheel.setBarColor(color);
        if (progressText != null && mProgressText != null) {
            mProgressText.setText(progressText);
            mProgressText.setVisibility(View.VISIBLE);
        }
    }
}
