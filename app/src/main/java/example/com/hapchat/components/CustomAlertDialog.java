package example.com.hapchat.components;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.hapchat.R;

public class CustomAlertDialog extends AppCompatDialog {

    private AlertDialog alertDialog;
    private TextView title_text;
    private TextView message_text;
    private TextView positiveButton;
    private TextView negativeButton;

    public CustomAlertDialog(Context context) {
        super(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View customView = inflater.inflate(R.layout.dialog_custom_box, null);

        positiveButton = (TextView) customView.findViewById(R.id.positiveButton);
        negativeButton = (TextView) customView.findViewById(R.id.negativeButton);
        title_text = (TextView) customView.findViewById(R.id.titleText);
        message_text = (TextView) customView.findViewById(R.id.textMessage);

        alertDialog = builder.setView(customView).create();
    }

    public void show() {
        if (alertDialog != null) alertDialog.show();
    }

    public void setTitle(String title) {
        title_text.setText(title);
    }

    public void setMessage(String message) {
        message_text.setText(message);
    }

    public void setPositiveButton(String positive_text, View.OnClickListener listener) {
        positiveButton.setText(positive_text);
        positiveButton.setOnClickListener(listener);
    }

    public void setNegativeButton(String negative_text, View.OnClickListener listener) {
        negativeButton.setText(negative_text);
        negativeButton.setOnClickListener(listener);
        negativeButton.setVisibility(View.VISIBLE);
    }

    public void close() {
        alertDialog.dismiss();
    }

}
