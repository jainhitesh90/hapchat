package example.com.hapchat.components;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;

import com.example.hapchat.R;

public class CustomToolbar extends AppBarLayout {

    private Toolbar mToolbar;

    public CustomToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_toolbar_layout, null);
        addView(view);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar_layout);
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getContext().getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        int color = typedValue.data;
        if (mToolbar != null)
            mToolbar.setBackgroundColor(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(0);
        }
    }

    public void setToolbarTitle(String text) {
        mToolbar.setTitle(text);
    }

    public void setSubtitle(String subtitle) {
        mToolbar.setSubtitle(subtitle);
    }

    public void setSubtitle(SpannableStringBuilder spannableStringBuilder) {
        mToolbar.setSubtitle(spannableStringBuilder);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

}
