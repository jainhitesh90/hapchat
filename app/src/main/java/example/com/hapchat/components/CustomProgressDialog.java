package example.com.hapchat.components;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hapchat.R;

public class CustomProgressDialog extends ProgressDialog {

    private Activity activity;
    private String message;
    private TextView textView;

    public CustomProgressDialog(Activity activity) {
        super(activity);
        this.activity = activity;
        setCancelable(false);
        setIndeterminate(true);
    }

    public CustomProgressDialog(Activity activity, String message) {
        super(activity);
        this.activity = activity;
        setCancelable(false);
        this.message = message;
        setIndeterminate(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.view_custom_progress, null);
        setContentView(view);
        ProgressBar progressWheel = (ProgressBar) view.findViewById(R.id.progress);
        textView = (TextView) findViewById(R.id.progress_text);
        IndeterminateProgressDrawable d = new IndeterminateProgressDrawable(getContext());
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getContext().getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        int color = typedValue.data;
        d.setTint(color);
        progressWheel.setProgressDrawable(d);
        progressWheel.setIndeterminateDrawable(d);

        if (message != null) {
            textView.setText(message);
        }
    }

    @Override
    public void setMessage(CharSequence message) {
        if (textView != null) {
            textView.setText(message);
        }
    }

    @Override
    public void show() {
        if (activity == null) return;
        if (!activity.isFinishing()) {
            try {
                super.show();
            } catch (Exception e){
                //LEAVE IT
            }
        }
    }

    @Override
    public void dismiss() {
        if (activity == null) return;
        if (!activity.isFinishing()) {
            super.dismiss();
        }
    }
}