package example.com.hapchat.presenter;

import android.content.Context;
import android.os.AsyncTask;

import com.example.hapchat.R;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import domain.entities.ChatList;
import domain.entities.ChatModel;
import domain.entities.ErrorModel;
import domain.rest.ChatRestCall;
import example.com.hapchat.MainThreadBus;
import example.com.hapchat.datasource.ChatDataSource;
import example.com.hapchat.tool.HelperClass;
import example.com.hapchat.tool.Utils;
import example.com.hapchat.ui.view.HomePageView;

public class HomePagePresenter extends Presenter<HomePageView> {

    private ChatRestCall chatRestCall = null;
    private List<ChatModel> mChatModelList = new ArrayList<>();

    public HomePagePresenter(Context context, HomePageView homePageView) {
        super(context, homePageView, new MainThreadBus());
        chatRestCall = new ChatRestCall(getRequestUrl(), getBus());
        mChatModelList = new ChatDataSource(getContext()).getAllChats();
    }

    public void getChatList() {
        if (Utils.isInternetConnected(getContext())) {
            chatRestCall.fetchChatList();
        } else if (mChatModelList.size() == 0)
            getView().noInternetConnection();
        else
            HelperClass.showMessage(getContext(), getContext().getString(R.string.fragment_error_page_title_no_internet_connection));
    }

    @Subscribe
    public void dataModelEvent(ChatList responseChatList) {
        List<ChatModel> chatModelList = responseChatList.getChatModelList();
        if (chatModelList != null && chatModelList.size() > 0) {
            new SaveChatListLocally(getContext(), chatModelList).execute();
        } else {
            HelperClass.showMessage(getContext(), getContext().getString(R.string.error_message_general));
        }
    }

    @Subscribe
    public void errorModelEvent(ErrorModel errorModel) {
        super.error(errorModel, true);
        if (mChatModelList.size() == 0)
            getView().onError();
        else
            HelperClass.showMessage(getContext(), getContext().getString(R.string.error_message_general));
    }

    private class SaveChatListLocally extends AsyncTask<String, Void, String> {
        Context context;
        List<ChatModel> chatModelList;

        SaveChatListLocally(Context context, List<ChatModel> chatModelList) {
            this.context = context;
            this.chatModelList = chatModelList;
        }

        @Override
        protected String doInBackground(String... params) {
            for (ChatModel chatModel : chatModelList) {
                try {
                    new ChatDataSource(getContext()).insert(chatModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return context.getString(R.string.done);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

        @Override
        protected void onPostExecute(String token) {
            getView().setData();
        }
    }
}