package example.com.hapchat.presenter;

import android.content.Context;

import com.squareup.otto.Bus;

import domain.entities.ErrorModel;
import domain.entities.RequestUrl;
import example.com.hapchat.tool.SettingsManager;
import example.com.hapchat.tool.Utils;

public abstract class Presenter<T> {

    private Context mContext;
    private T mView;
    private Bus mBus;
    private RequestUrl requestUrl;
    private boolean isRegister = false;

    public Presenter(Context context, T view, Bus bus) {
        this.mContext = context;
        this.mView = view;
        this.mBus = bus;
        requestUrl = new RequestUrl(SettingsManager.isDebuggable(), Utils.additionParams(context));
    }

    public void start() {
        if (!isRegister) {
            mBus.register(this);
            isRegister = true;
        }
    }

    public void stop() {
        try {
            if (isRegister) {
                mBus.unregister(this);
                isRegister = false;
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public Bus getBus() {
        return mBus;
    }

    public void setBus(Bus bus) {
        this.mBus = bus;
    }

    public T getView() {
        return mView;
    }

    public void setView(T view) {
        this.mView = view;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    public RequestUrl getRequestUrl() {
        return requestUrl;
    }

    public void error(ErrorModel errorModel) {
        error(errorModel, true);
    }

    public void error(ErrorModel errorModel, boolean showErrorMsg) {
        if (errorModel == null) return;
        if (showErrorMsg) {
            Utils.displayErrorMsg(getContext(), errorModel);
        }
    }

    public boolean isRegister() {
        return isRegister;
    }

    public void setIsRegister(boolean isRegister) {
        this.isRegister = isRegister;
    }

    public void checkBus() {
        if (!isRegister()) {
            start();
        }
    }
}