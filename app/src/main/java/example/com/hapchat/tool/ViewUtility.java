package example.com.hapchat.tool;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewUtility {

    public static void handleViewsVisibility(List<View> viewList, View... params) {
        List<Integer> idsToShow = new ArrayList<Integer>();
        if (params != null) {
            for (View param : params) {
                idsToShow.add(param.getId());
            }
        }
        for (View v : viewList) {
            if (idsToShow.contains(v.getId()))
                v.setVisibility(View.VISIBLE);
            else
                v.setVisibility(View.GONE);
        }
    }

    public static void loadImageFromURl(final Context context, String url, final ImageView imageView, final int placeholder) {
        Callback callback = new Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError() {
                Picasso.with(context).load(placeholder).placeholder(placeholder).into(imageView);
            }
        };

        try {
            if (url != null && !url.isEmpty()) {
                Picasso.with(context).load(url).placeholder(placeholder).fit().tag(context).into(imageView, callback);
            } else {
                Picasso.with(context).load(placeholder).placeholder(placeholder).into(imageView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}