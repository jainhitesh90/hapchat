package example.com.hapchat.tool;

import android.util.Log;

public class Logger {

    public static void i(String tag, String msg) {
        log(Log.INFO, tag, msg);
    }

    public static void d(String tag, String msg) {
        log(Log.DEBUG, tag, msg);
    }

    public static void v(String tag, String msg) {
        log(Log.VERBOSE, tag, msg);
    }

    public static void w(String tag, String msg) {
        log(Log.WARN, tag, msg);
    }

    public static void e(String tag, String msg) {
        log(Log.ERROR, tag, msg);
    }

    private static void log(int priority, String tag, String msg) {
        if (msg == null)
            msg = "no-MESSAGE";
        if (tag == null)
            tag = "empty-TAG";
        if (SettingsManager.isDebuggable()) {
            Log.println(priority, tag, msg);
        }
    }

    public static void exception(String tag, Throwable e) {
        if (e == null) return;
        if (e.getMessage() != null)
            log(Log.ERROR, tag, e.getMessage());
        if (SettingsManager.isDebuggable()) {
            e.printStackTrace();
        }
    }

}
