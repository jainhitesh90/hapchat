package example.com.hapchat.tool;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hapchat.R;

public class HelperClass {

    public static void showMessage(Context context, String message) {
        try {
            showSnackBar(context, message);
        } catch (Exception e) {
            showToastBar(context, message);
            String TAG = "HelperClass";
            Logger.exception(TAG, e);
        }
    }

    private static void showSnackBar(Context context, String message) {
        View rootView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT);
        snackbar.setActionTextColor(Color.WHITE);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.snackbar_background));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private static void showToastBar(Context context, String message) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.view_custom_toast_layout, null);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        int actionBarHeight = context.getResources().getDimensionPixelSize(R.dimen.design_toast_height);
        toast.setGravity(Gravity.BOTTOM, 0, actionBarHeight);
        toast.setView(layout);//setting the view of custom toast layout
        ((TextView) toast.getView().findViewById(R.id.custom_toast_message)).setText(message);
        toast.show();
    }
}
