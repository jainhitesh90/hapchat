package example.com.hapchat.tool;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.example.hapchat.BuildConfig;
import com.example.hapchat.R;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import domain.JsonKeys;
import domain.entities.ErrorModel;

public class Utils {

    private static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        if (useIPv4) {
                            if ((addr instanceof Inet4Address))
                                return addr.getHostAddress().toUpperCase();
                        } else {
                            if ((addr instanceof Inet6Address))
                                return addr.getHostAddress().toUpperCase();
                        }
                    }
                }
            }
        } catch (Exception e) {
            String TAG = "Utils";
            Logger.exception(TAG, e);
        }
        return "";
    }

    public static void displayErrorMsg(Context context, ErrorModel responseModel) {
        String defaultMsg = responseModel.getErrorMessage();
        switch (responseModel.getStatusCode()) {
            case 0:
                HelperClass.showMessage(context, context.getString(R.string.error_message_connection));
                break;
            case 200:
                String msg4 = defaultMsg != null ? defaultMsg : context.getString(R.string.error_message_general);
                HelperClass.showMessage(context, msg4);
                break;
            case 304:
                HelperClass.showMessage(context, context.getString(R.string.error_message_parameter));
                break;
            case 400:
                String msg3 = defaultMsg != null ? defaultMsg : context.getString(R.string.error_message_bad_request);
                HelperClass.showMessage(context, msg3);
                break;
            case 404:
                String msg1 = defaultMsg != null ? defaultMsg : context.getString(R.string.error_message_not_found);
                HelperClass.showMessage(context, msg1);
                break;
            case 422:
                String msg2 = defaultMsg != null ? defaultMsg : context.getString(R.string.error_message_unprocessbility);
                HelperClass.showMessage(context, msg2);
                break;
            case 500:
                HelperClass.showMessage(context, context.getString(R.string.error_message_internal));
                break;
            case 800:
                break;
            default:
                String msg5 = defaultMsg != null ? defaultMsg : context.getString(R.string.error_message_general);
                HelperClass.showMessage(context, msg5);
                break;
        }

    }

    private static String getDeviceId(Context context) {
        String aid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (aid == null) {
            TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            aid = TelephonyMgr.getDeviceId();
        }
        if (aid == null) {
            WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            aid = wm.getConnectionInfo().getMacAddress();
        }
        return aid;
    }

    public static HashMap<String, String> additionParams(Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put(JsonKeys.APP_VERSION, String.valueOf(BuildConfig.VERSION_CODE));
        params.put(JsonKeys.APP_IP_ADDRESS, Utils.getIPAddress(true));
        params.put(JsonKeys.USER_AGENT, "android");
        params.put(JsonKeys.USER_AGENT_VERSION, String.valueOf(android.os.Build.VERSION.SDK_INT));
        params.put(JsonKeys.APP_PACKAGE, BuildConfig.APPLICATION_ID);
        params.put(JsonKeys.DEVICE_ID, getDeviceId(context));
        return params;
    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }
}