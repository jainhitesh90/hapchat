package example.com.hapchat.tool;

import com.example.hapchat.BuildConfig;

public class SettingsManager {

    public static boolean isDebuggable() {
        return BuildConfig.BUILD_TYPE.equals("debug");
    }

}