package example.com.hapchat;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.example.hapchat.R;

import example.com.hapchat.tool.Logger;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppApplication extends Application {

    public AppApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/roboto_regular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
            );
        } catch (NoClassDefFoundError e) {
            Logger.exception(getClass().getSimpleName(), e);
        }
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
