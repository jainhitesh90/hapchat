package example.com.hapchat.wrapper;

import android.content.ContentValues;
import android.database.Cursor;

import domain.entities.ChatModel;
import example.com.hapchat.provider.ChatProvider;


public class ChatWrapper {

    public ChatWrapper() {
    }

    public static ContentValues modelToContent(ChatModel chatModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE, chatModel.getChatMessage());
        contentValues.put(ChatProvider.ChatColumns.COLUMN_CHAT_FAVORITE, chatModel.isMarkedFavorite());
        contentValues.put(ChatProvider.ChatColumns.COLUMN_USER_ID, chatModel.getUserId());
        contentValues.put(ChatProvider.ChatColumns.COLUMN_USER_NAME, chatModel.getUserName());
        contentValues.put(ChatProvider.ChatColumns.COLUMN_USER_IMAGE, String.valueOf(chatModel.getUserImage()));
        contentValues.put(ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE_TIME, chatModel.getChatMessageTime());
        return contentValues;
    }

    public static ChatModel cursorToModel(Cursor cursor) {
        ChatModel chatModel = new ChatModel();
        chatModel.setChatMessage(cursor.getString(cursor.getColumnIndex(ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE)));
        if (cursor.getInt(cursor.getColumnIndex(ChatProvider.ChatColumns.COLUMN_CHAT_FAVORITE)) == 1)
            chatModel.setMarkedFavorite(true);
        else
            chatModel.setMarkedFavorite(false);
        chatModel.setUserId(cursor.getString(cursor.getColumnIndex(ChatProvider.ChatColumns.COLUMN_USER_ID)));
        chatModel.setUserName(cursor.getString(cursor.getColumnIndex(ChatProvider.ChatColumns.COLUMN_USER_NAME)));
        chatModel.setUserImage(cursor.getString(cursor.getColumnIndex(ChatProvider.ChatColumns.COLUMN_USER_IMAGE)));
        chatModel.setChatMessageTime(cursor.getString(cursor.getColumnIndex(ChatProvider.ChatColumns.COLUMN_CHAT_MESSAGE_TIME)));
        return chatModel;
    }
}
