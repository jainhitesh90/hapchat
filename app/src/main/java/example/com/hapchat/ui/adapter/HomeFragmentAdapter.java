package example.com.hapchat.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.example.hapchat.R;

import example.com.hapchat.ui.fragment.ChatFragment;
import example.com.hapchat.ui.fragment.UsersFragment;

public class HomeFragmentAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public HomeFragmentAdapter(Context context, FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ChatFragment.newInstance(mContext);
            case 1:
                return UsersFragment.newInstance(mContext);
            default:
                return null;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.title_chats);
            case 1:
                return mContext.getString(R.string.title_users);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}