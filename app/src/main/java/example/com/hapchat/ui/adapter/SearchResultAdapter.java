package example.com.hapchat.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hapchat.R;

import domain.entities.ChatModel;
import example.com.hapchat.tool.HelperClass;
import example.com.hapchat.wrapper.ChatWrapper;

public class SearchResultAdapter extends CursorAdapter {

    private TextView message;
    private TextView userName;

    public SearchResultAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        ChatModel chatModel = ChatWrapper.cursorToModel(cursor);
        message.setText(chatModel.getChatMessage());
        userName.setText(" ~ " + chatModel.getUserName());
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_search_result, parent, false);
        message = (TextView) view.findViewById(R.id.message);
        userName = (TextView) view.findViewById(R.id.user_name);
        return view;
    }

    @Override
    public int getCount() {
        if (getCursor() == null || super.getCount() == 0) {
            HelperClass.showMessage(mContext, mContext.getString(R.string.error_message_general));
            return 0;
        } else
            return super.getCount();
    }
}