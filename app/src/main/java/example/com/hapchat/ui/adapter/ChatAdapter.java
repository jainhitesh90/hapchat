package example.com.hapchat.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.hapchat.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import domain.entities.ChatModel;
import example.com.hapchat.datasource.ChatDataSource;
import example.com.hapchat.tool.HelperClass;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatHolder> {

    private Context mContext;
    private List<ChatModel> mChatModelList;

    public ChatAdapter(Context context, List<ChatModel> chatModelList) {
        this.mContext = context;
        this.mChatModelList = chatModelList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ChatHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_chat, viewGroup, false);
        return new ChatHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatHolder cardHolder, final int position) {
        final ChatModel chatModel = mChatModelList.get(position);
        cardHolder.userName.setText(chatModel.getUserName());
        Character initial = chatModel.getUserName().charAt(0);
        setColor(cardHolder.userName, initial);
        cardHolder.chatMessage.setText(chatModel.getChatMessage());

        //since time received in api is not in epoch, just displaying the time in HH:MM format
        String timeInShort = chatModel.getChatMessageTime().substring(11, 16);
        cardHolder.chatMessageTime.setText(timeInShort);

        cardHolder.markFavorite.setOnCheckedChangeListener(null);
        cardHolder.markFavorite.setChecked(chatModel.isMarkedFavorite());
        cardHolder.markFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    HelperClass.showMessage(mContext, "Added To favorite");
                else
                    HelperClass.showMessage(mContext, "Removed from favorite");
                mChatModelList.get(position).setMarkedFavorite(isChecked);
                new ChatDataSource(mContext).update(chatModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mChatModelList.size();
    }

    static class ChatHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.user_name)
        TextView userName = null;

        @Bind(R.id.chat_message)
        TextView chatMessage = null;

        @Bind(R.id.chat_message_time)
        TextView chatMessageTime = null;

        @Bind(R.id.add_to_favorite)
        CheckBox markFavorite;

        ChatHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void setColor(TextView tv, Character character) {
        int color;
        switch (character) {
            case 'J':
                color = R.color.user_color_1;
                break;
            case 'R':
                color = R.color.user_color_2;
                break;
            case 'S':
                color = R.color.user_color_3;
                break;
            case 'M':
                color = R.color.user_color_4;
                break;
            default:
                color = R.color.user_color_5;
                break;
        }
        tv.setTextColor(ContextCompat.getColor(mContext, color));
    }
}
