package example.com.hapchat.ui.widgets;

import android.content.Context;
import android.support.annotation.IntDef;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.hapchat.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.Bind;
import butterknife.ButterKnife;

public class StateRelativeLayout extends RelativeLayout {

    public static final int NO_INTERNET_CONNECTION = 0;
    public static final int SOMETHING_WENT_WRONG = 1;
    @Bind(R.id.state_image_view)
    ImageView mImageView;
    @Bind(R.id.error_title)
    TextView mTitle;
    @Bind(R.id.error_message)
    TextView mMessage;
    @Bind(R.id.button_retry)
    Button mRetryButton;

    public StateRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = inflater.inflate(R.layout.widget_state_relative_layout, this, true);
        ButterKnife.bind(this, mView);
    }

    public StateRelativeLayout(Context context) {
        super(context, null);
    }

    public void displayStateView(@StateType int stateType) {
        switch (stateType) {
            case NO_INTERNET_CONNECTION:
                setStateValues(R.drawable.ic_state_no_internet_connection
                        , R.string.fragment_error_page_title_no_internet_connection
                        , R.string.fragment_error_page_message_no_internet_connection
                        , R.string.fragment_error_page_button_retry);
                break;
            case SOMETHING_WENT_WRONG:
                setStateValues(R.drawable.ic_state_something_went_wrong
                        , R.string.fragment_error_page_title_something_went_wrong
                        , R.string.fragment_error_page_message_something_went_wrong
                        , R.string.fragment_error_page_button_retry);
                break;
        }
    }

    private void setStateValues(int stateImageResourceId
            , int stateHeadingStringResourceId
            , int stateSubHeadingStringResourceId
            , int stateButtonRetryStringResourceId) {
        mImageView.setImageResource(stateImageResourceId);

        if (stateHeadingStringResourceId != 0) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText(stateHeadingStringResourceId);
        } else {
            mTitle.setVisibility(View.GONE);
        }

        if (stateButtonRetryStringResourceId != 0) {
            mRetryButton.setVisibility(View.VISIBLE);
            mRetryButton.setText(stateButtonRetryStringResourceId);
        } else {
            mRetryButton.setVisibility(View.GONE);
        }

        if (stateSubHeadingStringResourceId != 0) {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(stateSubHeadingStringResourceId);
        } else {
            mMessage.setVisibility(View.GONE);
        }
    }

    public void setOnRetryButtonClickListener(OnClickListener clickListener) {
        mRetryButton.setOnClickListener(clickListener);
    }

    @Retention(RetentionPolicy.CLASS)
    @IntDef({NO_INTERNET_CONNECTION, SOMETHING_WENT_WRONG})
    public @interface StateType {
    }
}
