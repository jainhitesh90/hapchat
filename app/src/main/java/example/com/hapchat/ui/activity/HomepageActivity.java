package example.com.hapchat.ui.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import com.example.hapchat.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import domain.entities.ChatModel;
import example.com.hapchat.components.CustomAlertDialog;
import example.com.hapchat.components.CustomProgressDialog;
import example.com.hapchat.components.CustomToolbar;
import example.com.hapchat.datasource.ChatDataSource;
import example.com.hapchat.presenter.HomePagePresenter;
import example.com.hapchat.tool.HelperClass;
import example.com.hapchat.tool.Utils;
import example.com.hapchat.tool.ViewUtility;
import example.com.hapchat.ui.adapter.SearchResultAdapter;
import example.com.hapchat.ui.fragment.HomeFragment;
import example.com.hapchat.ui.view.HomePageView;
import example.com.hapchat.ui.widgets.StateRelativeLayout;

public class HomepageActivity extends ParentActivity implements NavigationView.OnNavigationItemSelectedListener, HomePageView {

    @Bind(R.id.toolbar)
    protected CustomToolbar mToolbar;

    @Bind(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;

    @Bind(R.id.navigation_drawer)
    protected NavigationView mNavigationView;

    @Bind(R.id.content_frame)
    protected FrameLayout homeContent;

    @Bind(R.id.state_relative_layout)
    protected StateRelativeLayout mStateRelativeLayout;

    @Bind({R.id.state_relative_layout, R.id.content_frame})
    protected List<View> mViewList;

    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private static final String NAV_ITEM_ID = "navItemId";
    private final Handler mDrawerActionHandler = new Handler();
    private int mCount = 0, mNavItemId;
    private Context mContext;
    private ActionBarDrawerToggle mDrawerToggle;
    private HomeFragment homeFragment;
    private SearchResultAdapter searchResultAdapter;
    private SearchView searchView = null;
    private List<ChatModel> chatModelList;
    private ChatDataSource chatDataSource;
    private HomePagePresenter mHomePagePresenter;
    private CustomProgressDialog mCustomProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        mContext = this;
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar.getToolbar());
        if (null == savedInstanceState) {
            mNavItemId = R.id.navigation_item_home;
        } else {
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }
        initialize();
    }

    private void initialize() {
        chatDataSource = new ChatDataSource(mContext);
        mHomePagePresenter = new HomePagePresenter(mContext, this);
        mHomePagePresenter.start();
        mCustomProgressDialog = new CustomProgressDialog(this);
        homeFragment = new HomeFragment();
        setUpNavigationDrawer();
        addHeader();
        chatModelList = chatDataSource.getAllChats();
        getChats();
    }

    private void setUpNavigationDrawer() {
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.getMenu().findItem(mNavItemId).setChecked(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar.getToolbar(), R.string.activity_home_drawer_open, R.string.activity_home_drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        navigate(mNavItemId);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void getChats() {
        if (chatModelList.size() != 0) {
            showHomeFragment();
        }
        mHomePagePresenter.getChatList();
    }

    private void navigate(final int itemId) {
        switch (itemId) {
            case R.id.navigation_item_home:
                break;
            case R.id.navigation_item_refresh:
                if (Utils.isInternetConnected(mContext)) {
                    showProgressDialog();
                    mHomePagePresenter.getChatList();
                }
                break;
            case R.id.navigation_item_share:
                shareApp();
                break;
            case R.id.navigation_item_contact:
                emailUs();
                break;
            case R.id.navigation_item_about_us:
                aboutUs();
                break;
        }
    }

    private void aboutUs() {
        final CustomAlertDialog aboutUsDialog = new CustomAlertDialog(mContext);
        aboutUsDialog.setPositiveButton(getResources().getString(R.string.button_ok), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aboutUsDialog.close();
            }
        });
        aboutUsDialog.setTitle(getResources().getString(R.string.title_about_us));
        aboutUsDialog.setMessage(getResources().getString(R.string.message_about_us));
        aboutUsDialog.show();
    }

    private void shareApp() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType(getResources().getString(R.string.app_share_text_type));
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_share_title));
        sharingIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.app_share_msg));
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.app_share_text)));
    }

    private void emailUs() {
        Intent gmail = new Intent(Intent.ACTION_VIEW);
        gmail.setClassName(getResources().getString(R.string.gmail_package_name), getResources().getString(R.string.gmail_class_name));
        gmail.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.contact_email)});
        gmail.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.contact_email_title));
        gmail.setType(getResources().getString(R.string.app_share_text_type));
        gmail.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.contact_email_msg));
        startActivity(gmail);
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        menuItem.setChecked(true);
        mNavItemId = menuItem.getItemId();
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    private void addHeader() {
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.view_drawer_header, mNavigationView, false);
        mNavigationView.addHeaderView(header);
    }

    private void showHomeFragment() {
        hideProgressDialog();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, homeFragment).commitAllowingStateLoss();
        ViewUtility.handleViewsVisibility(mViewList, homeContent);
    }

    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (mCount == 1) {
            mCount = 0;
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        } else {
            HelperClass.showMessage(mContext, getResources().getString(R.string.activity_home_toast_press_again_to_exit));
            mCount++;
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.length() > 0) {
                    Cursor cursor = new ChatDataSource(mContext).getSearchCursor(query);
                    searchResultAdapter = new SearchResultAdapter(mContext, cursor);
                    searchView.setSuggestionsAdapter(searchResultAdapter);
                    return true;
                } else {
                    searchView.setSuggestionsAdapter(null);
                    return true;
                }
            }
        });
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                return false;
            }
        });
        return true;
    }

    @Override
    protected void onResume() {
        if (searchView != null) {
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
            ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(searchView.getWindowToken(), 0);
        }
        super.onResume();
    }

    @Override
    public void setData() {
        chatModelList = chatDataSource.getAllChats();
        if (chatModelList.size() > 0) {
            HelperClass.showMessage(mContext, "Chat List updated");
            showHomeFragment();
        } else {
            ViewUtility.handleViewsVisibility(mViewList, mStateRelativeLayout);
            mStateRelativeLayout.displayStateView(StateRelativeLayout.SOMETHING_WENT_WRONG);
            mStateRelativeLayout.setOnRetryButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getChats();
                }
            });
        }
    }

    @Override
    public void onError() {
        ViewUtility.handleViewsVisibility(mViewList, mStateRelativeLayout);
        mStateRelativeLayout.displayStateView(StateRelativeLayout.SOMETHING_WENT_WRONG);
        mStateRelativeLayout.setOnRetryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getChats();
            }
        });
    }

    @Override
    public void noInternetConnection() {
        ViewUtility.handleViewsVisibility(mViewList, mStateRelativeLayout);
        mStateRelativeLayout.displayStateView(StateRelativeLayout.NO_INTERNET_CONNECTION);
        mStateRelativeLayout.setOnRetryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getChats();
            }
        });
    }

    private void showProgressDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.dismiss();
        }
    }
}