package example.com.hapchat.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hapchat.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import domain.entities.UserModel;
import example.com.hapchat.components.LoadingProgressView;
import example.com.hapchat.datasource.ChatDataSource;
import example.com.hapchat.ui.adapter.UserAdapter;

public class UsersFragment extends Fragment {

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Bind(R.id.loading)
    LoadingProgressView loadingProgressView;

    private Context mContext;
    private UserAdapter userAdapter;
    private List<String> stringList;
    private List<UserModel> userModelList = new ArrayList<>();
    private ChatDataSource chatDataSource;

    public static UsersFragment newInstance(Context context) {
        UsersFragment nearMeFragment = new UsersFragment();
        nearMeFragment.mContext = context;
        return nearMeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = container.getContext();
        View view = inflater.inflate(R.layout.fragment_user, null);
        ButterKnife.bind(this, view);

        chatDataSource = new ChatDataSource(mContext);
        stringList = chatDataSource.getAllUsers();
        return view;
    }

    private void getUserData() {
        userModelList.clear();
        for (String userName : stringList) {
            UserModel userModel = new UserModel();
            userModel.setUserName(userName);
            userModel.setUserImage(chatDataSource.getUserImage(userName));
            userModel.setFavoriteCount(chatDataSource.getAllFavoriteChatFromUser(userName));
            userModel.setTotalMessageCount(chatDataSource.getAllChatFromUser(userName));
            userModelList.add(userModel);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showProgress();
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);
            getUserData();
            if (userAdapter == null) {
                userAdapter = new UserAdapter(mContext, userModelList);
                mRecyclerView.setAdapter(userAdapter);
            } else {
                userAdapter.notifyDataSetChanged();
            }
            hideProgress();
        }
    }

    private void hideProgress() {
        loadingProgressView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        loadingProgressView.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }
}
