package example.com.hapchat.ui.view;

public interface HomePageView {

    void setData();

    void onError();

    void noInternetConnection();
}