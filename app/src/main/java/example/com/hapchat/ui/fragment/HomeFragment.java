package example.com.hapchat.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hapchat.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import domain.entities.ChatModel;
import example.com.hapchat.datasource.ChatDataSource;
import example.com.hapchat.ui.adapter.HomeFragmentAdapter;

public class HomeFragment extends Fragment {

    @Bind(R.id.sliderTabLayout)
    protected TabLayout mSliderTabLayout;

    @Bind(R.id.viewpager)
    protected ViewPager mViewPager;

    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = container.getContext();
        View view = inflater.inflate(R.layout.fragment_home, null);
        ButterKnife.bind(this, view);
        ChatDataSource chatDataSource = new ChatDataSource(mContext);
        List<ChatModel> mChatModelList = chatDataSource.getAllChats();
        if (mChatModelList.size() != 0)
            setData();

        return view;
    }

    private void setData() {
        HomeFragmentAdapter homeFragmentAdapter = new HomeFragmentAdapter(mContext, getFragmentManager());
        mViewPager.setAdapter(homeFragmentAdapter);
        mSliderTabLayout.setupWithViewPager(mViewPager);
        int tabPosition = 0;
        TabLayout.Tab tab = mSliderTabLayout.getTabAt(tabPosition);
        if (tab != null) {
            tab.select();
        }
    }
}
