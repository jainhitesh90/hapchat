package example.com.hapchat.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.hapchat.R;

public class SplashScreenActivity extends ParentActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_page);
        mContext = this;
    }

    @Override
    public void onStart() {
        super.onStart();
        new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (SplashScreenActivity.this.isFront()) {
                        startActivity(new Intent(mContext, HomepageActivity.class));
                    }
                }
            }
        }.start();
    }
}
