package example.com.hapchat.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hapchat.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import domain.entities.UserModel;
import example.com.hapchat.tool.CircularImageView;
import example.com.hapchat.tool.ViewUtility;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder> {

    private Context mContext;
    private List<UserModel> mUserModelList;

    public UserAdapter(Context context, List<UserModel> userModelList) {
        this.mContext = context;
        this.mUserModelList = userModelList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user, viewGroup, false);
        return new UserHolder(v);
    }

    @Override
    public void onBindViewHolder(UserHolder cardHolder, final int position) {
        final UserModel userModel = mUserModelList.get(position);
        cardHolder.userName.setText(userModel.getUserName());
        ViewUtility.loadImageFromURl(mContext, userModel.getUserImage(), cardHolder.userImage, R.drawable.ic_placeholder_user_image);
        cardHolder.favoriteCount.setText(String.valueOf(userModel.getFavoriteCount()));
        cardHolder.totalMsg.setText(String.valueOf(userModel.getTotalMessageCount()));
    }

    @Override
    public int getItemCount() {
        return mUserModelList.size();
    }

    static class UserHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.user_image)
        CircularImageView userImage = null;

        @Bind(R.id.user_name)
        TextView userName = null;

        @Bind(R.id.total_msg)
        TextView totalMsg = null;

        @Bind(R.id.favorite_count)
        TextView favoriteCount = null;

        UserHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
