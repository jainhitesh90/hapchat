package example.com.hapchat.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hapchat.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import domain.entities.ChatModel;
import example.com.hapchat.datasource.ChatDataSource;
import example.com.hapchat.ui.adapter.ChatAdapter;

public class ChatFragment extends Fragment {

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private Context mContext;
    private List<ChatModel> mChatModelList;

    public static ChatFragment newInstance(Context context) {
        ChatFragment chatFragment = new ChatFragment();
        chatFragment.mContext = context;
        return chatFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = container.getContext();
        View view = inflater.inflate(R.layout.fragment_chats, null);
        ButterKnife.bind(this, view);

        mChatModelList = new ChatDataSource(mContext).getAllChats();
        setItems();
        return view;
    }

    private void setItems() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ChatAdapter chatAdapter = new ChatAdapter(mContext, mChatModelList);
        mRecyclerView.setAdapter(chatAdapter);
    }
}
